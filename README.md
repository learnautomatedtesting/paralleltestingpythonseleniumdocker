# Setting Up Parallel Testing with Python, Cucumber, and Docker Compose

In this guide, we will walk you through the steps to set up parallel testing for your Python-based test suite using Cucumber as the test framework and Docker Compose for managing a Selenium Grid. Parallel testing allows you to distribute and run tests concurrently, improving test execution speed.

## Prerequisites

- Python installed on your system.
- Docker and Docker Compose installed.

## Steps

### 1. Set Up Selenium Grid with Docker Compose

Create a `docker-compose.yml` file to define your Selenium Grid configuration with multiple nodes (e.g., Chrome, Firefox). Here's a sample `docker-compose.yml`:

```yaml
version: '3'
services:
  chrome:
    image: selenium/node-chrome:4.13.0-20231004
    # Configuration for Chrome node

  firefox:
    image: selenium/node-firefox:4.13.0-20231004
    # Configuration for Firefox node

  selenium-hub:
    image: selenium/hub:4.13.0-20231004
    # Configuration for Selenium Hub
````

```bash
Copy code
docker-compose up -d
```

### 2. Install Required Libraries
Install the necessary Python libraries for your project using pip. For example, you may need pytest, pytest-bdd, and selenium.
### 3. Write Parallel Test Scenarios
Write your test scenarios using Cucumber and pytest-bdd. Organize your tests into separate feature files or scenarios that you want to run in parallel.

### 4. Configure Parallel Test Execution
For pytest with pytest-xdist:
Install the pytest-xdist plugin using pip:

### 5. Create a pytest configuration file (e.g., pytest.ini) with the following content to specify parallelization options:

```ini
[pytest]
addopts = -n auto
```

###  6. Execute Parrallel tests

setup the allure report local

https://gitlab.com/learnautomatedtesting/devtoolsperformancereporter

Part 2
### Integrate Allure with Jenkins:

1. Install Allure Jenkins Plugin:
**Go to Jenkins Dashboard > Manage Jenkins > Manage Plugins > Available.**
Search for Allure Jenkins Plugin and install it.
2. Configure Allure Commandline:
**Navigate to Manage Jenkins > Global Tool Configuration.**
Under Allure Commandline, add Allure Commandline with a name (e.g., "allure").
3. Jenkins Job Configuration:
**Under Build, execute your tests to produce Allure results.**
In Post-build Actions, add "Allure Report" and specify the results directory (/path/to/allure-results).

### Send Report via Email in Jenkins:
1. Install Email Extension Plugin:
Navigate to Manage Jenkins > Manage Plugins.
Install the Email Extension Plugin.
2. Jenkins Job Configuration:
Go to Post-build Actions > Editable Email Notification.
In Attachments, set the pattern to attach files (e.g., /path/to/allure-report/*.html for all HTML files in the Allure report).
Configure recipients, subject, and other email settings.
3. SMTP Configuration:
If you haven't already configured an SMTP server in Jenkins:

Navigate to Manage Jenkins > Configure System.
Scroll to the Email Notification section.
Fill in your SMTP server details, the email suffix, and any other necessary details. Test the configuration by sending a test email.



www.linkedin.com/comm/mynetwork/discovery-see-all?usecase=PEOPLE_FOLLOWS&followMember=ralphvanderhorst
