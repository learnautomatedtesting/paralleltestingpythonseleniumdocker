from pytest_bdd import scenarios, given, when, then
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

# Load the feature file
scenarios('../features/sample.feature')

# The Selenium WebDriver instance
driver = None

@given('I am using the selenium grid with "<browser_name>"')
def setup_selenium_grid(browser_name):
    global driver
    
    if browser_name == "chrome":
        try:
            chrome_options = webdriver.ChromeOptions()
            # Example Chrome option
            
            driver = webdriver.Remote(
                command_executor="http://localhost:4444/wd/hub",
                options=chrome_options,  # Add Chrome options here
                keep_alive=False,  # Disable HTTP keep-alive
                file_detector=None,  # Use the default file detector
            )
        except Exception as e:
            print(f"WebDriver version: {driver.capabilities['browserVersion']}")
            print(f"An unexpected error occurred: {e}")
    elif browser_name == "firefox":
        try:
            driver = webdriver.Remote(
                command_executor="http://localhost:4444/wd/hub",
                options=webdriver.FirefoxOptions(),  # Add Firefox options here
            )
        except Exception as e:
            print(f"An unexpected error occurred: {e}")

@when("I visit Google")
def visit_google():
    global driver
    driver.get("https://www.google.com")

@then("Google homepage should be displayed")
def google_homepage_displayed():
    assert "Google" in driver.title
    driver.quit()
