Feature: Visit Google with different browsers

  Scenario: Visiting Google with Chrome
    Given I am using the selenium grid with "<browser_name>"
    When I visit Google
   
     Examples:
      | browser_name |
      | chrome       |